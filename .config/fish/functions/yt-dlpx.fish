function yt-dlpx
    # downloads the highest available audio quality, converts to mp3, and formats title
    # use like yt-dlpx 'link'
    yt-dlp --embed-thumbnail -f bestaudio -x --audio-format mp3 $argv[1] -o "%(title)s.%(ext)s" 
end
