function convert-music
    set dir "~/downloads/test/"
    cd $dir
    for file in **/*.mp3 **/*.flac
        set output (path change-extension '' $file)
        ffmpeg -i $file -codec:a libmp3lame -qscale:a 2 -y -map_metadata 0 -id3v2_version 3 -write_id3v1  1 $output.mp3
        rm $file
    end
end