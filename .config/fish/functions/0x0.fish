function 0x0
    # allows you to easily send files to 0x0 with 
    # 0x0 'file.txt'
    curl -F"file=@$argv[1]" https://0x0.st
end
