function clean
    # this currently hangs when you try to remove any packages
    # so I just use it to fetch a list of unneded ones and copy paste it into a zypper rm command
    sudo zypper packages --unneeded | awk -F'|' 'NR==0 || NR==1 || NR==2 || NR==3 || NR==4 {next} {print $3}' | grep -v Name | sudo xargs -r zypper remove --clean-deps
end
