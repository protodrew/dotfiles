# UNFINISHED
# requires, pandoc & yq
# assumes markdown file has yaml elements titled 
#   "aliases" "type", "tags", "date-created", & "date-modified" (no double quotes)
# generates a (mostly) compliant atom feed
#   TODO
#       add a way to get `summary` element for feed entries
#       process-wikilinks .md extension into .html
function obsidian-ssg
    set vault_path /home/protodrew/projects/protowiki/
    set site_path /home/protodrew/projects/protosite/
    set html_template lib/templates/template.html

    set note_file $site_path"rss/notes.xml"
    touch $note_file
    rm $note_file
    # Create the notes Atom file with the required header
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" >>"$note_file"
    echo "<feed xmlns=\"http://www.w3.org/2005/Atom\">" >>"$note_file"
    echo "  <title>proto's notes</title>" >>"$note_file"
    echo "  <id>tag:proto.garden:notes</id>" >>"$note_file"
    echo "  <link href=\"https://proto.garden/notes/\"/>" >>"$note_file"
    echo "  <link href=\"https://proto.garden/rss/notes.xml\" rel=\"self\"/>" >>"$note_file"
    echo "  <updated>$(date -u +%Y-%m-%dT%H:%M:%S)Z</updated>" >>"$note_file"
    set blog_file $site_path"rss/blog.xml"
    touch $blog_file
    rm $blog_file
    # Create the blog Atom file with the required header
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" >>"$blog_file"
    echo "<feed xmlns=\"http://www.w3.org/2005/Atom\">" >>"$blog_file"
    echo "  <title>proto's blog</title>" >>"$blog_file"
    echo "  <id>tag:proto.garden:blog</id>" >>"$blog_file"
    echo "  <link href=\"https://proto.garden/blog/\"/>" >>"$blog_file"
    echo "  <link href=\"https://proto.garden/rss/blog.xml\" rel=\"self\"/>" >>"$blog_file"
    echo "  <updated>$(date -u +%Y-%m-%dT%H:%M:%S)Z</updated>" >>"$blog_file"


    obsidian-export $vault_path $site_path

    cd $site_path
    for file in **/*.md
        set page_name (path change-extension 'html' $file)
        set tempfile (string trim $file)
        set file $tempfile


        set title (string sub -s 4 \"(yq -e -N --front-matter=extract '.aliases' $file)[1]\")
        set path (path change-extension '' $file)
        set created (yq -e -N --front-matter=extract '.date-created' $file) # YYYY-MM-DDTHH:MM:SS
        set created_short (string sub -l 10 $created) # YYYY-MM-DD
        set modified (yq -e -N --front-matter=extract '.date-modified' $file) # YYYY-MM-DDTHH:MM:SS
        set type (yq -e -N --front-matter=extract '.type' $file)
        set tags (yq -e -N --front-matter=extract '.tags' $file)
        pandoc $file --standalone --template $html_template --metadata title="$title" -o "$site_path$page_name"

        if not string match -q index $type # TODO: still lets a few index files slip thru


            switch $type
                case blog
                    set atom_file $blog_file
                case note
                    set atom_file $note_file
            end


            echo "  <entry>" >>"$atom_file"
            echo "    <title>$title</title>" >>"$atom_file"
            echo "    <summary>$summary</summary>" >>"$atom_file"
            echo "    <published>$created""Z""</published>" >>"$atom_file"
            echo "    <updated>$modified""Z""</updated>" >>"$atom_file"
            echo "    <link href=\"https://proto.garden/$page_name\"/>" >>"$atom_file"
            echo "    <id>tag:proto.garden,$created_short,$path</id>" >>"$atom_file"
            echo "    <author>" >>"$atom_file"
            echo "        <name>proto</name>" >>"$atom_file"
            echo "        <email>proto@proto.garden</email>" >>"$atom_file"
            echo "    </author>" >>"$atom_file"
            echo "  </entry>" >>"$atom_file"

        end
    end
    echo "</feed> " >>$note_file
    echo "</feed> " >>$blog_file
end