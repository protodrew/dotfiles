#!/usr/bin/env bash
set -euxo pipefail

# designed to be used on openSUSE Tumbleweed from the "base system (or w/e)" base config

# add packman repo and switch all available packages
sudo zypper ar http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman
zypper addrepo https://download.opensuse.org/repositories/games:tools/openSUSE_Tumbleweed/games:tools.repo
sudo zypper refresh
sudo zypper dup --from packman --allow-vendor-change

# install sway and other basic applications (some might already be installed)
sudo zypper -n install patterns-openSUSEway fuzzel foot kvantum-qt5 papirus-icon-theme papirus-folders
# install modern-unix tools
sudo zypper -n install bat eza fd duf the_silver_searcher neovim neofetch
# install my fav commandline programs
sudo zypper -n install newsboat lynx mosh yt-dlp ffmpeg pandoc tlp
# install graphical applications
sudo zypper -n install nautilus firefox spotify-easyrpm discord kdenlive audacity godot imv mpv syncthing
# gaming stuff
sudo zypper -n install steam lutris retroarch steamtricks winetricks

# copy .config dotfiles

git clone 'https://codeberg.org/protodrew/dotfiles/' ~/dotfiles
cp -r ~/dotfiles/.config ~/
sudo cp -r ~/dotfiles/etc/ /etc/ #for gtkgreet

# gtk theme setup
wget -P ~/temp/ "https://github.com/EliverLara/Nordic/releases/download/v2.2.0/Nordic.tar.xz"
sudo mkdir /usr/share/themes/Nordic/
sudo tar -xf ~/temp/Nordic.tar.xz -C /usr/share/themes/
gsettings set org.gnome.desktop.interface gtk-theme "Nordic"
gsettings set org.gnome.desktop.wm.preferences theme "Nordic"
echo 'GTK_THEME="Nordic"' | sudo tee -a /etc/environment

# qt theme setup TODO: fix (currently nonfunctional)
git clone 'https://github.com/tonyfettes/materia-nord-kvantum.git' ~/temp/materia-nord-kvantum/
sudo mv ~/temp/materia-nord-kvantum/Kvantum /usr/share/Kvantum

sudo systemctl enable syncthing@$USER.service
sudo systemctl start syncthing@$USER.service
sudo systemctl set-default graphical.target
sudo systemctl enable greetd
sudo chsh -s $(which fish)
