#!/usr/bin/env bash
set -euxo pipefail

configs="amfora BetterDiscord mako neofetch sway foot fish fuzzel fontconfig"

cp ~/.config/discord/settings.json ~/documents/dotfiles/.config/discord/settings.json
cp -r ~/scripts/ ~/documents/dotfiles/
for config in $configs; do
    cp -r ~/.config/$config ~/documents/dotfiles/.config/
done
