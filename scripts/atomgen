#!/usr/bin/env bash
set -euxo pipefail

# Check if directory path is provided
if [ $# -ne 1 ]; then
  echo "Usage: $0 <directory>"
  exit 1
fi

# Directory path provided as argument
directory=$1

# Output filename
output_file="atom.xml"

# Start generating atom.xml
echo '<?xml version="1.0" encoding="UTF-8"?>' >"$output_file"
echo '<feed xmlns="http://www.w3.org/2005/Atom">' >>"$output_file"
echo '  <title>My Blog</title>' >>"$output_file"
echo '  <link href="https://blog.protodrew.website/atom.xml" rel="self"/>' >>"$output_file"
echo '  <updated>'$(date -u +"%Y-%m-%dT%H:%M:%SZ")'</updated>' >>"$output_file"

# Loop through HTML files in the directory
for file in "$directory"/*.html; do
  # Extract file name without extension
  filename=$(basename "$file" .html)

  # Get creation and modification dates
  created=$(stat -c %y "$file" | awk '{print $1}')
  modified=$(stat -c %y "$file" | awk '{print $1}')

  # Generate entry for each file
  echo '  <entry>' >>"$output_file"
  echo '    <title>'"$filename"'</title>' >>"$output_file"
  echo '    <created>'"$created"'</created>' >>"$output_file"
  echo '    <modified>'"$modified"'</modified>' >>"$output_file"
  echo '    <link href="https://blog.protodrew.website/'"$filename"'.html"/>' >>"$output_file"
  echo '  </entry>' >>"$output_file"
done

# Close feed tag
echo '</feed>' >>"$output_file"

echo "atom.xml generated successfully!"
