#!/usr/bin/env bash
set -euxo pipefail

# run with ./feedgen.sh /path/to/folder "Website title" "https://example.com" "https://example.com/feed.xml"
#               no trailing slash  ----^  [OPTIONAL]      [OPTIONAL]             [OPTIONAL]
# Specify the folder to scan
folder="$1"

# Specify the output Atom file
atom_file="output.xml"

# Specify the Atom title and link, feel free to hardcode paths
# if you don't plan on using this with other scripts
atom_title=$2 #defaults to "Example feed"
atom_link=$3  # defaults to https://example.com
atom_self=$4  # defaults to $atomlink/atom.xml

# Check if the folder is provided as an argument
if [ -z "$folder" ]; then
  echo "Usage: $0 folder"
  exit 1
fi

if [ -z "$atom_title" ]; then
  atom_title="Example feed"
fi

if [-z "$atom_link" ]; then
  atom_link="https://example.com"
fi

if [-z "$atom_link" ]; then
  atom_self="$atom_link/atom.xml"
fi

# Check if the folder exists
if [ ! -d "$folder" ]; then
  echo "Folder '$folder' does not exist."
  exit 1
fi

# Create the initial Atom file with the required header
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" >"$atom_file"
echo "<feed xmlns=\"http://www.w3.org/2005/Atom\">" >>"$atom_file"
echo "  <title>$atom_title</title>" >>"$atom_file"
echo "  <link href=\"$atom_link\"/>" >>"$atom_file"
echo " <link href=\"$atom_self\" rel="self"/>" >>"$atom_file"
echo "  <updated>$(date -u +"%Y-%m-%dT%H:%M:%SZ")</updated>" >>"$atom_file"

# Iterate through each HTML file in the folder
for file in "$folder"/*.html; do
  # Check if any HTML files are found
  [ -e "$file" ] || break

  # Extract the first paragraph as a summary
  #summary=$(awk -F'</\?p[^>]*>' 'NF{print $2; exit}' "$file")
  summary=$(sed -n '/<p>/,/<\/p>/p' "$file" | sed 's/<[^>]*>//g' | sed '/^\s*$/d' | head -n 1)
  # Get the created and modified dates of the file
  created=$(stat -c %W "$file")
  modified=$(stat -c %Y "$file")

  # Check if the created date already exists and is older than the file's created date
  existing_created=$(stat -c %W "$file.atom" 2>/dev/null)
  if [ -n "$existing_created" ] && [ "$existing_created" -lt "$created" ]; then
    created="$existing_created"
  fi

  # Update the created and modified dates
  touch -d "@$created" "$file"
  touch -d "@$modified" "$file"

  # Generate the Atom entry
  echo "  <entry>" >>"$atom_file"
  echo "    <title>$(basename "$file" .html)</title>" >>"$atom_file"
  echo "    <summary>$summary</summary>" >>"$atom_file"
  echo "    <published>$(date -u -d "@$created" +"%Y-%m-%dT%H:%M:%SZ")</published>" >>"$atom_file"
  echo "    <updated>$(date -u -d "@$modified" +"%Y-%m-%dT%H:%M:%SZ")</updated>" >>"$atom_file"
  echo "    <link href=\"$atom_link/$(basename "$file")\"/>" >>"$atom_file"
  echo "    <id>$atom_link/$(basename "$file")</id>" >>"$atom_file"
  echo "  </entry>" >>"$atom_file"

  echo "Processed: $file"
done

# Close the Atom feed
echo "</feed>" >>"$atom_file"

echo "Script execution completed. Atom file generated: $atom_file"
