#!/usr/bin/env bash
# a simple program to play mp3 streams from the commandline, minimal output, minimal functionality

# dependencies
# socat
# mpv

# arguments
# comfy (plays radiofreefedi comfy station)
# fedi (plays radiofreefedi main station)
# now-playing (gets artist and track name)
# end (kills all mpv sessions (for now), sometimes needs to be run twice??)

# TODO
# - option to have new songs displayed as notifications
# - option to configure custom stations
# - process arguments and handle errors in a smart way :p

set -euxo pipefail

socket_path="/tmp/mpvsocket"

case "$1" in
comfy) # plays the radiofreefedi comfy station
    mpv "https://solid55.streamupsolutions.com:2199/tunein/bpqyoykj-comfy.pls" --no-cache --really-quiet --input-ipc-server="$socket_path" &
    mpv_pid=$!
    ;;

fedi) # plays the radiofreefedi station
    mpv "https://solid1.streamupsolutions.com:2199/tunein/vfuucmgt.pls" --no-cache --really-quiet --input-ipc-server=/tmp/mpvsocket &
    mpv_pid=$!
    ;;

now-playing) # gets currently playing song title (only tested with above stations)
    json_output=$(echo '{"command": ["get_property", "media-title"]}' | socat - "$socket_path")
    media_name=$(echo "$json_output" | jq -r '.data')
    echo "currently playing: " "$media_name"
    ;;

end) # find and kill mpv session, TODO: make it use mpv_pid instead of killing all mpv sessions
    pgrep 'mpv' | xargs kill
    ;;

*) # should accept arbitrary url and run it the same **UNTESTED**
    mpv "$1" --no-cache --really-quiet --input-ipc-server=/tmp/mpvsocket &
    mpv_pid=$!
    ;;
esac
