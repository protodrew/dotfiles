# protodrew's dotfiles
I now host my dotfiles on [git.merveilles.town](https://git.merveilles.town/proto/dotfiles)
![](desktop-1.webp)
![](desktop-2.webp)

These are all the configs I use for my personal linux setup. Mostly centered around [sway](https://swaywm.org), the [everforest](https://github.com/sainnhe/everforest) color scheme, and openSUSE Tumbleweed.

## quickstart

copy the .config folders to ~/
copy the etc folder to your root directory (only if using greetd as your login manager)
```
cp -r .config/ ~/
cp -r etc/ /
```
add the repositories to your tumbleweed system with
```
sudo zypper ar -r repositories.repo
```

for the obsidian config, copy `.obsidian/` to your vault folder path

## scripts

the scripts are all work in progress and provided as-is, I am mostly either using them for works in progress, or keeping them like scrap cars to harvest when I forget how to do a for loop in bash...